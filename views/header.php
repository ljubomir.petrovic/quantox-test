<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.0.min.js"></script>
    <script src="js/event_counter.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<ul class="menu">
    <li class="menu-item"><a href="index.php">Create new event</a></li>
    <li class="menu-item"><a href="index.php?data&format=json">JSON</a></li>
    <li class="menu-item"><a href="index.php?data&format=xml">XML</a></li>
    <li class="menu-item"><a href="index.php?data&format=csv">CSV</a></li>
</ul>
<div class="container">