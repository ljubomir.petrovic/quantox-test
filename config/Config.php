<?php

namespace Config;

class Config
{
    private $db_host = '';
    private $db_user = '';
    private $db_password = '';
    private $db_name = '';

    public function __construct()
    {
        $this->parseConfig();
    }

    public function parseConfig()
    {
        $config_file = '../.config';
        $content = file_get_contents($config_file);
        $params = explode("\n", $content);

        foreach ($params as $param) {
            if (preg_match('/^db_host=(.*?)$/', $param, $match)) {
                $this->db_host = isset($match[1]) ? $match[1] : '';
            } elseif (preg_match('/^db_user=(.*?)$/', $param, $match)) {
                $this->db_user = isset($match[1]) ? $match[1] : '';
            } elseif (preg_match('/^db_password=(.*?)$/', $param, $match)) {
                $this->db_password = isset($match[1]) ? $match[1] : '';
            } elseif (preg_match('/^db_name=(.*?)$/', $param, $match)) {
                $this->db_name = isset($match[1]) ? $match[1] : '';
            }
        }
    }

    /**
     * @return string
     */
    public function getDBHost()
    {
        return $this->db_host;
    }

    /**
     * @return string
     */
    public function getDBName()
    {
        return $this->db_name;
    }

    /**
     * @return string
     */
    public function getDBUser()
    {
        return $this->db_user;
    }

    /**
     * @return string
     */
    public function getDBPassword()
    {
        return $this->db_password;
    }
}