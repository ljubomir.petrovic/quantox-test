<?php

require __DIR__ . '/bootstrap/autoload.php';

use Core\Http\Dispatcher;

$routes = [
    'GET' => [
        '' => ['EventPageController' => 'index'],
        'data' => ['EventDataController' => 'render'],
    ],
    'POST' => [
        'start_counter' => ['EventCounterController' => 'render'],
    ]
];

Dispatcher::handleRequest($routes);
