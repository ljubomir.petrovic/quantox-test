<?php

namespace App\Helper;

class DataFormatHelper
{
    /**
     * @param \SimpleXMLElement $xml
     * @param array $data
     */
    public static function toXml(\SimpleXMLElement $xml, array $data)
    {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                $key = 'item'.$key;
            }
            if( is_array($value) ) {
                $subnode = $xml->addChild($key);
                self::toXml($subnode, $value);
            } else {
                $xml->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    /**
     * @param $data
     * @return string
     */
    public static function toCsv($data)
    {
        $out = '"event_name","country_code","counter"<br/>';
        foreach($data as $val) {

            $out .= '"' . $val['event_name'] . '","' . $val['country_code'] . '","' . $val['counter'] . '"<br/>';

        }
        
        return $out;
    }
}