<?php

namespace App\Interfaces;

use Core\Http\Request;

interface EventInterface
{
    public function render(Request $request);
}