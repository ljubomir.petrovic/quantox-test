<?php

namespace App\Controller;

use Core\Http\Request;
use App\Services\EventsCounterService;
use App\Interfaces\EventInterface;

class EventCounterController implements EventInterface
{
    /**
     * @param Request $request
     */
    public function render(Request $request)
    {
        $data = json_decode($request->getField('data'), true);

        $country = $data['country'];
        $event = $data['event'];

        $events_counter_service = new EventsCounterService();
        echo $events_counter_service->storeEventsCounter($event, $country);
    }
}