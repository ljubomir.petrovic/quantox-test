<?php

namespace App\Controller;

use App\View\View;
use App\Model\Countries;
use App\Model\Events;

class EventPageController
{
    public function index()
    {
        $countries = new Countries();
        $countries_data = $countries->all();

        $events = new Events();
        $events_data = $events->all();
        View::make('event', ['form_data' => ['countries' => $countries_data, 'events' => $events_data]]);
    }
}