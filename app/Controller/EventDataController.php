<?php

namespace App\Controller;

use App\View\View;
use Core\Http\Request;
use App\Services\EventsCounterService;
use App\Interfaces\EventInterface;

class EventDataController implements EventInterface
{
    /**
     * @param Request $request
     */
    public function render(Request $request)
    {
        $format = $request->getField('format');
        $events_counter_service = new EventsCounterService();
        $result = $events_counter_service->getEventsCounterData($format);

        View::make('statistic', ['statistic' => $result]);
    }
}