<?php

namespace Core\Http;

class Router
{
    private static $method = '';
    private static $controller = '';

    /**
     * @param $ctl_method
     */
    public static function setControllerMethod($ctl_method)
    {
        $ctl_name = key($ctl_method);

        if (isset($ctl_method[$ctl_name])) {
            self::$method = $ctl_method[$ctl_name];
        }

        self::$controller = 'App\\Controller\\' . $ctl_name;
    }

    /**
     * @return string
     */
    public static function getMethod()
    {
        return self::$method;
    }

    /**
     * @return mixed
     */
    public static function getController()
    {
        return self::$controller;
    }
}