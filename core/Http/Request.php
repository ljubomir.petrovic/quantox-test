<?php

namespace Core\Http;

class Request
{
    private $request_method = [];
    private $vars = [];
    private $routes = [];

    /**
     * Http constructor.
     */
    public function __construct()
    {
        $this->request_method = $_SERVER['REQUEST_METHOD'];
        $this->setVars();
    }

    /**
     * @return array
     */
    public function getMethod()
    {
        return $this->request_method;
    }

    public function setVars()
    {
        foreach ($_POST as $key => $val) {

            $this->vars[$key] = $val;
        }
        $i = 0;

        foreach ($_GET as $key => $val) {

            if ($i == 0) {

                $this->routes[] = $key;
                $i++;
            }
            $this->vars[$key] = $val;

        }
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->vars;
    }

    /**
     * @param string $field
     * @return mixed
     */
    public function getField($field)
    {
        if (isset($this->vars[$field])) {
            return $this->vars[$field];
        }

        return '';
    }

    /**
     * @return mixed|string
     */
    public function currentRoute()
    {
        return isset($this->routes[0]) ? $this->routes[0] : '';
    }
}
