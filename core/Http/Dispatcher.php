<?php

namespace Core\Http;

class Dispatcher
{
    public static function handleRequest($routes)
    {
        $request = new Request();
        $request_method = $request->getMethod();
        $request_route = $request->currentRoute();

        if (isset($routes[$request_method][$request_route])) {
            Router::setControllerMethod($routes[$request_method][$request_route]);
            $controller = Router::getController();
            $method = Router::getMethod();

            if (!class_exists($controller)) {
                exit('Controller: ' . $controller . ' doesn\'t exists');
            }
            $controller_obj = new $controller();

            if (!method_exists($controller_obj, $method))
            {
                exit('Controller method: ' . $method . ' doesn\'t exists');
            }
            $controller_obj->{$method}($request);
        }

    }
}